from turtle import update
from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125, null=True)
    author = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name + " by " + self.author